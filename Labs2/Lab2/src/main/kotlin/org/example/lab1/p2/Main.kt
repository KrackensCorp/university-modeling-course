package org.example.lab1.p2

private const val ITERATION_COUNT = 1000000
private val probability1 = doubleArrayOf(0.0714, 0.3810, 0.4286, 0.1143, 0.0048)
private val probability2 = doubleArrayOf(0.1762, 0.3132, 0.2937, 0.1678, 0.0489)

private fun getDistribution(probability: DoubleArray): DoubleArray {
    val distribution = DoubleArray(probability.size)
    repeat(ITERATION_COUNT) {
        val value = Math.random()
        probability.foldIndexed(0.0) { index, acc, prob ->
            if (value in acc..(acc + prob)) {
                distribution[index]++
            }
            prob
        }
    }
    return distribution
}

private fun printDistribution(distribution: DoubleArray, probability: DoubleArray) {
    distribution.foldIndexed(0.0) { index, acc, dist ->
        val prop = probability[index]
        val range = acc.round(3)..(acc + prop).round(3)
        println("$range \t->\t $dist")
        prop
    }
}

private fun Double.round(decimals: Int): Double {
    var multiplier = 1.0
    repeat(decimals) { multiplier *= 10 }
    return kotlin.math.round(this * multiplier) / multiplier
}

fun main(args: Array<String>) {
    val distribution = getDistribution(probability2)
    printDistribution(distribution, probability2)
    println()

    val probability = DoubleArray(distribution.size)
    repeat(probability.size) { index ->
        probability[index] = distribution[index] / ITERATION_COUNT
    }
    val newDistribution = getDistribution(probability)
    printDistribution(newDistribution, probability)
}