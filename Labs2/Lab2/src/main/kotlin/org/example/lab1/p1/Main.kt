package org.example.lab1.p1

import org.apache.poi.ss.util.CellRangeAddress
import org.apache.poi.xddf.usermodel.chart.*
import org.apache.poi.xddf.usermodel.chart.AxisPosition.*
import org.apache.poi.xddf.usermodel.chart.ChartTypes.*
import org.apache.poi.xddf.usermodel.chart.MarkerStyle.*
import org.apache.poi.xddf.usermodel.chart.XDDFDataSourcesFactory.*
import org.apache.poi.xssf.usermodel.*
import java.io.FileOutputStream
import kotlin.math.round
import kotlin.math.sqrt

private const val ITERATION_COUNT = 1000000

private const val  step = 0.025
private val arr = DoubleArray((2 / step).toInt())

private fun putValue(value: Double) {
    for (j in arr.indices) {
        if (value > j * step && value < (j + 1) * step) {
            arr[j]++
        }
    }
}

private fun getArray(): DoubleArray {
    var x: Double
    var y: Double
    for (i in 0 until ITERATION_COUNT) {
        y = Math.random() * 2.5 - 1
        if (y >= 0 && y < 0.125) {
            x = 2 * sqrt(2.0) * sqrt(y)
            putValue(x)
        }
        if (y >= -0.25 && y < 0.625) {
            x = (4 + sqrt(40 * y + 11)) / 5
            putValue(x)
        }
    }
    return arr
}

@ExperimentalStdlibApi
fun main() {
    draw(
        sheetName = "LineChart",
        filename = "data.xlsx",
        dataSet = func(),
    )
}

@ExperimentalStdlibApi
private fun func(): Map<ClosedFloatingPointRange<Double>, Double> {
    return buildMap {
        getArray().forEachIndexed { index, item ->
            this[getRange(index, step)] = item
        }
    }
}

private fun getRange(index: Int, step: Double): ClosedFloatingPointRange<Double> =
    ((index * step * 100) / 100).round(2)..(((index + 1) * step * 100) / 100).round(2)

private fun Double.round(decimals: Int): Double {
    var multiplier = 1.0
    repeat(decimals) { multiplier *= 10 }
    return round(this * multiplier) / multiplier
}

private fun draw(sheetName: String, dataSet: Map<ClosedFloatingPointRange<Double>, Double>, filename: String) {
    XSSFWorkbook().use { wb ->
        with(wb.createSheet(sheetName)) {
            createDataTable(dataSet)
            createDataChart(dataSet)
        }
        FileOutputStream(filename).use { fileOut -> wb.write(fileOut) }
    }
}

private fun XSSFSheet.createDataTable(dataSet: Map<ClosedFloatingPointRange<Double>, Double>) {
    with(Pair(createRow(0), createRow(1))) {
        var index = 0
        dataSet.forEach { (interval, data) ->
            first.createCell(index).setCellValue(interval.toString())
            second.createCell(index).setCellValue(data)
            index++
        }
    }
}

private fun XSSFSheet.createDataChart(dataSet: Map<ClosedFloatingPointRange<Double>, Double>) {
    val chart = initChart("Data")
    val data = chart.initData(dataSet, "X", "Y", this)
    chart.plot(data)
}

private fun XSSFChart.initData(
    dataSet: Map<ClosedFloatingPointRange<Double>, Double>,
    titleX: String,
    titleY: String,
    xssfSheet: XSSFSheet,
): XDDFChartData {
    val bottomAxis = createCategoryAxis(BOTTOM).apply { setTitle(titleX) }
    val leftAxis = createValueAxis(LEFT).apply { setTitle(titleY) }

    val data = createData(LINE, bottomAxis, leftAxis) as XDDFChartData
    data.setVaryColors(false)

    val lastCol = dataSet.size - 1
    val categoryCellRangeAddress = CellRangeAddress(0, 0, 0, lastCol)
    val categoryDataSource = fromStringCellRange(xssfSheet, categoryCellRangeAddress)

    val numericalCellRangeAddress = CellRangeAddress(1, 1, 0, lastCol)
    val numericalDataSource = fromNumericCellRange(xssfSheet, numericalCellRangeAddress)

    with(data.addSeries(categoryDataSource, numericalDataSource) as XDDFLineChartData.Series) {
        isSmooth = false
        setMarkerStyle(DOT)
        setMarkerSize(3)
    }
    return data
}

private fun XSSFSheet.initChart(title: String): XSSFChart {
    val chart = with(createDrawingPatriarch()) {
        val anchor = createAnchor(0, 0, 0, 0, 0, 4, 7, 26)
        createChart(anchor)
    }
    chart.setTitleText(title)
    chart.titleOverlay = false
    return chart
}
