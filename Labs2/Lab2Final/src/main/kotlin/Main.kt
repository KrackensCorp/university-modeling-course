import java.util.*

val maxNumberOfVertices = 0
var numberOfVertices = 0
var MIN_NUMBER_OF_VERTICES = 1
var MAX_NUMBER_OF_VERTICES = 500
var PROBABILITY = 0.5

fun main() {
    //val rtg = RandomTreeGenerator(50)
    val root: Node = generationWithDegree(5)
    val render = RenderTree()
    render.render(root)
}

fun RandomTreeGenerator() {
    setMaxNumberOfVertices(10)
}

fun RandomTreeGenerator(maxNumberOfVertices: Int) {
    setMaxNumberOfVertices(maxNumberOfVertices)
}

fun setMaxNumberOfVertices(maxNumberOfVertices: Int) {
    var maxNumberOfVertices = maxNumberOfVertices

    if (maxNumberOfVertices < MIN_NUMBER_OF_VERTICES) maxNumberOfVertices =
        MIN_NUMBER_OF_VERTICES
    if (maxNumberOfVertices > MAX_NUMBER_OF_VERTICES) maxNumberOfVertices =
        MAX_NUMBER_OF_VERTICES
}

fun generationWithDegree(degree: Int): Node {
    val root = Node().nodes(null)
    val lvl1 = ArrayList<Node>()
    lvl1.add(root)
    root.nodeAtLevels.add(lvl1)
    root.nextGeneration(GenerationType.DEGREE, degree, root)
    return root
}

fun generationWithDepth(depth: Int): Node {
    val root = Node().nodes(null)
    val lvl1 = ArrayList<Node>()
    lvl1.add(root)
    root.nodeAtLevels.add(lvl1)
    root.nextGeneration(GenerationType.DEPTH, depth, root)
    return root
}









































































