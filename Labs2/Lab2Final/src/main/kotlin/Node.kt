import java.util.*

class Node {
    var parent: Node? = null
    var children = ArrayList<Node>()
    var number = 0

    private var level = 1

    var nodeAtLevels = ArrayList<ArrayList<Node>>()

    fun nextGeneration(
        genType: GenerationType,
        value: Int,
        root: Node
    ) {
        when (genType) {
            GenerationType.DEGREE -> {
                while (numberOfVertices < maxNumberOfVertices && nodeAtLevels.size == level) {
                    val levelNodeList = root.nodeAtLevels[root.level - 1]
                    for (i in 0..levelNodeList.size) {
                        var j = 0
                        while (j < value
                            && numberOfVertices < maxNumberOfVertices
                        ) {
                            if (Math.random() < PROBABILITY) {
                                levelNodeList[i].children.add(nodes(levelNodeList[i]))
                                if (root.nodeAtLevels.size < root.level + 1) {
                                    root.nodeAtLevels.add(ArrayList())
                                }
                                root.nodeAtLevels[level].add(levelNodeList[i].children[levelNodeList[i].children.size - 1])
                            }
                            j++
                        }
                    }
                    root.level++
                }
            }

            GenerationType.DEPTH -> {
                while (level < value && nodeAtLevels.size == level) {
                    val levelNodeList = root.nodeAtLevels[root.level - 1]

                    for (i in levelNodeList.indices) {
                        for (j in 0..4) {
                            if (Math.random() < PROBABILITY) {
                                levelNodeList[i].children.add(nodes(levelNodeList[i]))
                                if (root.nodeAtLevels.size < root.level + 1) {
                                    root.nodeAtLevels.add(ArrayList())
                                }
                                root.nodeAtLevels[level].add(levelNodeList[i].children[levelNodeList[i].children.size - 1])
                            }
                        }
                    }
                    root.level++
                }
            }
        }
    }

    fun nodes(parent: Node?) {
        this.parent = parent

        numberOfVertices++
        this.number = numberOfVertices
    }

}
