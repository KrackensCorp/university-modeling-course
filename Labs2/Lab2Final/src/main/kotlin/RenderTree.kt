import org.graphstream.graph.*
import org.graphstream.graph.implementations.*
import org.graphstream.ui.swing_viewer.SwingViewer
import org.graphstream.ui.swing_viewer.ViewPanel
import org.graphstream.ui.view.Viewer
import java.awt.*
import java.awt.event.*
import java.lang.String
import javax.swing.*


class RenderTree {
    private val cameraSpeed = 4.0

    private var type = GenerationType.DEGREE

    private fun createConnections(graph: Graph, node: Node) {
        graph.addNode(String.valueOf(node.number))
            .setAttribute("label", String.valueOf(node.number))
        graph.addEdge(
            String.valueOf(node.parent!!.number) + String.valueOf(node.number),
            String.valueOf(node.parent!!.number), String.valueOf(node.number)
        )
        for (i in node.children.indices) {
            createConnections(graph, node.children[i])
        }
    }

    var panel = JPanel()

    private fun generate(frame: JFrame, value: Int, count: Int, probability: Double) {
        var view_panel: ViewPanel

        panel.layout = GridLayout()

        frame.add(panel, BorderLayout.CENTER)

        val graph: Graph = SingleGraph("Tree")

        PROBABILITY = probability

        val root: Node = if (type === GenerationType.DEGREE) {
            generationWithDegree(value)
        } else {
            generationWithDepth(value)
        }

        graph.setAttribute("ui.stylesheet", styleSheet)
        graph.setAttribute("ui.antialias")
        graph.setAttribute("ui.quality")

        graph.addNode(String.valueOf(root.number))
            .setAttribute("label", String.valueOf(root.number))

        var i = 0
        while (i < root.children.size) {
            createConnections(graph, root.children[i])
            i++
        }

        val viewer = SwingViewer(graph, Viewer.ThreadingModel.GRAPH_IN_ANOTHER_THREAD)
        viewer.enableAutoLayout()
        view_panel = viewer.addDefaultView(false) as ViewPanel
        val rec = panel.bounds
        view_panel.setBounds(0, 0, rec.width, rec.height)
        view_panel.preferredSize = Dimension(rec.width, rec.height)
        panel.add(view_panel)

        view_panel.addMouseWheelListener { mwe ->
            zoomGraphMouseWheelMoved(
                mwe,
                view_panel
            )
        }

        view_panel.addKeyListener(object : KeyListener {
            override fun keyTyped(e: KeyEvent?) {}


            override fun keyPressed(e: KeyEvent?) {
                when (e!!.keyCode) {
                    KeyEvent.VK_W -> {
                        val x = view_panel.camera.viewCenter.x
                        val y: Double =
                            view_panel.camera.viewCenter.y + cameraSpeed * view_panel.camera.viewPercent
                        val z = view_panel.camera.viewCenter.z
                        view_panel.camera.setViewCenter(x, y, z)
                    }
                    KeyEvent.VK_A -> {
                        val x: Double =
                            view_panel.camera.viewCenter.x - cameraSpeed * view_panel.camera.viewPercent
                        val y = view_panel.camera.viewCenter.y
                        val z = view_panel.camera.viewCenter.z

                        view_panel.camera.setViewCenter(x, y, z)
                    }
                    KeyEvent.VK_S -> {
                        val x = view_panel.camera.viewCenter.x
                        val y: Double =
                            view_panel.camera.viewCenter.y - cameraSpeed * view_panel.camera.viewPercent
                        val z = view_panel.camera.viewCenter.z

                        view_panel.camera.setViewCenter(x, y, z)
                    }
                    KeyEvent.VK_D -> {
                        val x: Double =
                            view_panel.camera.viewCenter.x + cameraSpeed * view_panel.camera.viewPercent
                        val y = view_panel.camera.viewCenter.y
                        val z = view_panel.camera.viewCenter.z

                        view_panel.camera.setViewCenter(x, y, z)
                    }
                }
            }

            override fun keyReleased(e: KeyEvent?) {}
        })
    }

    fun render(tree: Node?) {
        System.setProperty("org.graphstream.ui", "swing")

        val frame = JFrame()
        frame.layout = BorderLayout()
        frame.defaultCloseOperation = WindowConstants.EXIT_ON_CLOSE
        frame.setBounds(0, 0, 700, 500)
        frame.preferredSize = Dimension(700, 500)

        val label1 = JLabel("Значение основного параметра:")
        val valueField = JTextField("5")

        val label2 = JLabel("Количество вершин:")
        val countField = JTextField("50")

        val label3 = JLabel("Вероятность появления вершины:")
        val probabilityField = JTextField("0.5")

        valueField.columns = 5
        countField.columns = 5
        probabilityField.columns = 5

        frame.add(panel, BorderLayout.CENTER)

        generate(
            frame,
            valueField.text.toInt(),
            countField.text.toInt(),
            probabilityField.text.toDouble()
        )

        val paramPanel = JPanel()
        frame.add(paramPanel, BorderLayout.EAST)

        val rbtnDegree = JRadioButton("Генерация по степени вершин")
        rbtnDegree.isSelected = true
        val rbtnDepth = JRadioButton("Генерация по глубине дерева")

        val group = ButtonGroup()

        val button = JButton("Генерация")

        button.addActionListener {
            frame.remove(panel)
            panel = JPanel()
            generate(
                frame,
                valueField.text.toInt(),
                countField.text.toInt(),
                probabilityField.text.toDouble()
            )
            frame.paint(frame.graphics)
            frame.isVisible = true
        }

        paramPanel.preferredSize = Dimension(300, frame.height)

        paramPanel.add(rbtnDegree)
        paramPanel.add(rbtnDepth)
        paramPanel.add(label1)
        paramPanel.add(valueField)
        paramPanel.add(label2)
        paramPanel.add(countField)
        paramPanel.add(label3)
        paramPanel.add(probabilityField)
        paramPanel.add(button)

        group.add(rbtnDegree)
        group.add(rbtnDepth)

        rbtnDegree.setBounds(400, 0, 250, 50)
        rbtnDepth.setBounds(700, 0, 250, 50)

        frame.isVisible = true

        rbtnDegree.addActionListener {
            type = GenerationType.DEGREE
            countField.isEnabled = true
        }

        rbtnDepth.addActionListener {
            type = GenerationType.DEPTH
            countField.isEnabled = false
        }
    }

    fun zoomGraphMouseWheelMoved(mwe: MouseWheelEvent, view_panel: ViewPanel) {
        if (Event.ALT_MASK != 0) {
            if (mwe.wheelRotation > 0) {
                val new_view_percent = view_panel.camera.viewPercent + 0.05
                view_panel.camera.viewPercent = new_view_percent
            } else if (mwe.wheelRotation < 0) {
                val current_view_percent = view_panel.camera.viewPercent
                if (current_view_percent > 0.05) {
                    view_panel.camera.viewPercent = current_view_percent - 0.05
                }
            }
        }
    }

    private val styleSheet = (""
            + "graph {"
            + "	canvas-color: white; "
            + "	fill-mode: plain; "
            + "	fill-color: black; "
            + "	padding: 60px; "
            + "}"
            + ""
            + "node {"
            + "	shape: box;"
            + "	size: 10px;"
            + "	size-mode: fit;"
            + "	fill-color: #FFF;"
            + "	stroke-mode: plain;"
            + "	stroke-color: grey;"
            + "	stroke-width: 3px;"
            + "	padding: 5px, 1px;"
            + "	shadow-mode: none;"
            + "	text-style: normal;"
            + "	text-font: 'Droid Sans';"
            + "}"
            + ""
            + "node:clicked {"
            + "	stroke-mode: plain;"
            + "	stroke-color: red;"
            + "}"
            + ""
            + "node:selected {"
            + "	stroke-mode: plain;"
            + "	stroke-color: blue;"
            + "}"
            + ""
            + "edge {"
            + "	shape: freeplane;"
            + "	size: 3px;"
            + "	fill-color: grey;"
            + "	fill-mode: plain;"
            + "	shadow-mode: none;"
            + "	shadow-color: rgba(0,0,0,100);"
            + "	shadow-offset: 3px, -3px;"
            + "	shadow-width: 0px;"
            + "	arrow-shape: arrow;"
            + "	arrow-size: 20px, 6px;"
            + "}")
}






































