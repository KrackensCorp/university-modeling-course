public class Main {
    public static int ITERATION_COUNT = 1000000;

    static double[] probability1 = new double[] {0.0714, 0.3810, 0.4286, 0.1143, 0.0048};
    static double[] probability2 = new double[] {0.1762, 0.3132, 0.2937, 0.1678, 0.0489};

    public static int[] getDistribution(double[] probability) {
        int[] distribution = new int[probability.length];

        double value;

        for (int i = 0; i < ITERATION_COUNT; i++) {
            value = Math.random();

            double sum = 0.0;

            for (int j = 0; j < probability.length; j++) {
                if (value >= sum && value <= sum + probability[j]) {
                    distribution[j]++;
                }
                sum += probability[j];
            }
        }

        double sum2 = 0.0;

        for (int i = 0; i < distribution.length; i++) {

            System.out.println((double) Math.round(sum2 * 1000) / 1000 + " - "
                    + (double) Math.round((sum2 + probability[i]) * 1000) / 1000 + ":\t\t " + distribution[i]);

            sum2 += probability[i];
        }

        System.out.println();

        return distribution;
    }

    public static void main(String[] args) {
        int[] distribution = getDistribution(probability2);

        double[] probability = new double[distribution.length];

        for (int i = 0; i < probability.length; i++) {
            probability[i] = (double)distribution[i] / ITERATION_COUNT;
        }

        int[] newDistribution = getDistribution(probability);
    }
}