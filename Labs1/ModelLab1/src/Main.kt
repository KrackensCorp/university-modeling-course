import kotlin.math.roundToLong
import kotlin.math.sqrt

var step = 0.025
var arr = IntArray((2 / step).toInt())

fun main() {
    PaintDistribution().PaintDistribution(getArray())
}

fun putValue(value: Double) {
    for (j in arr.indices) {
        if (value > j * step && value < (j + 1) * step) {
            arr[j]++
        }
    }
}

fun getArray(): IntArray {
    var interval: Double
    var ksi: Double
    var X: Double

    for (i in 0..1000000) {
        interval = Math.random()
        ksi = Math.random()
        if (interval >= 0 && interval < 0.125) {
            X = sqrt(ksi)
            putValue(X)
        }
        if (interval in 0.125..1.0) {
            if (ksi > 0.0575) {
                X = (3 + 7 * sqrt(10 * ksi / 7 - 19 / 196)) / 5
                putValue(X)
            }
        }
    }
    for (i in arr.indices) {
        println(
            ((i * step * 100).roundToLong()
                .toDouble() / 100).toString() + " - " + ((i + 1) * step * 100).roundToLong()
                .toDouble() / 100 + ":" + "  \t " + arr[i]
        )
    }
    return arr
}
