import java.awt.Graphics
import javax.swing.ButtonGroup
import kotlin.math.max
import java.awt.*
import javax.swing.BoundedRangeModel
import javax.swing.DefaultBoundedRangeModel
import javax.swing.JFrame
import kotlin.math.roundToLong

class PaintDistribution : JFrame() {
    lateinit var distributionArray: IntArray

    private fun maxDistributionValue(): Int {
        var max: Int = distributionArray[0]

        for (i in arr.indices) {
            max = max(max, distributionArray.get(i))
        }
        return max
    }

    override fun paint(graphics: Graphics) {
        super.paint(graphics)
        this.layout = null
        val group = ButtonGroup()
        this.isVisible = true

        val model: BoundedRangeModel =
            DefaultBoundedRangeModel(10000000, 1000000, 1000000, 50000000)

        for (i in arr.indices) {
            graphics.color = Color(50 + i / 10, 200 - i / 10, 50 + i / 10);
            //graphics.color = Color(0 + i / 10, 0 + i / 10, 0 + i / 10) черный
            val height =
                (distributionArray[i].toDouble() / (maxDistributionValue().toDouble() / 500)).roundToLong()
                    .toInt()
            graphics.drawRect(
                90 + (500 / (1 / step) * i).toInt(),
                590 - height,
                (500 / (1 / step)).toInt(),
                height
            )
        }
        graphics.color = Color.BLACK

        /* Y */graphics.drawLine(90, 90, 90, 591)
        graphics.drawLine(90, 90, 85, 100)
        graphics.drawLine(90, 90, 95, 100)

        /* X */graphics.drawLine(90, 591, 1120, 591)
        graphics.drawLine(1120, 591, 1110, 586)
        graphics.drawLine(1120, 591, 1110, 596)
        graphics.drawString(maxDistributionValue().toString(), 80, 80)
        graphics.drawString(2.toString(), 1090, 615)
        graphics.drawLine(1090, 586, 1090, 596)
        graphics.drawString(1.0.toString(), 590, 615)
        graphics.drawLine(590, 586, 590, 596)
        graphics.drawString(0.toString(), 90, 615)
    }

    fun PaintDistribution(array: IntArray) {
        super.setTitle("А я вот нарисовал еще")
        distributionArray = array
        setDefaultLookAndFeelDecorated(true)
        defaultCloseOperation = EXIT_ON_CLOSE
        pack()
        setSize(1200, 700)
        isVisible = true
    }
}