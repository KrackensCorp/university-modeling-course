import kotlin.math.roundToLong

var probabilityNum = doubleArrayOf(0.0714, 0.3810, 0.4286, 0.1143, 0.0048)
var probabilityNum2 = doubleArrayOf(0.1762, 0.3132, 0.2937, 0.1678, 0.0489)

fun main() {
    val distribution: IntArray = getDistribution(probabilityNum)
    val probability = DoubleArray(distribution.size)

    for (i in probability.indices) {
        probability[i] = distribution[i].toDouble() / 1000000
    }
    println(getDistribution(probability))
}

fun getDistribution(probability: DoubleArray): IntArray {
    val distribution = IntArray(probability.size)
    var value: Double
    for (i in 0..1000000) {
        value = Math.random()
        var sum = 0.0
        for (j in probability.indices) {
            if (value >= sum && value <= sum + probability[j]) {
                distribution[j]++
            }
            sum += probability[j]
        }
    }
    var sum2 = 0.0
    for (i in distribution.indices) {
        println(
            ((sum2 * 1000).roundToLong().toDouble() / 1000).toString() + " - "
                    + ((sum2 + probability[i]) * 1000).roundToLong()
                .toDouble() / 1000 + ":" + "    \t" + distribution[i]
        )
        sum2 += probability[i]
    }
    return distribution
}