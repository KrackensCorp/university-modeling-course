import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class PaintDistribution extends JFrame {
    private int[] distributionArray;

    private int minDistributionValue() {
        int min = distributionArray[0];

        for (int i = 0; i < Main.arr.length; i++) {
            min = Math.min(min, distributionArray[i]);
        }

        return min;
    }

    private int maxDistributionValue() {
        int max = distributionArray[0];

        for (int i = 0; i < Main.arr.length; i++) {
            max = Math.max(max, distributionArray[i]);
        }

        return max;
    }

    public void paint(Graphics graphics) {
        super.paint(graphics);

        this.setLayout(null);
        ButtonGroup group = new ButtonGroup();
        this.setVisible(true);

        BoundedRangeModel model =
                new DefaultBoundedRangeModel(10000000, 1000000, 1000000, 50000000);


        for (int i = 0; i < Main.arr.length; i++) {
            //graphics.setColor(new Color(50 + i / 10, 200 - i / 10, 50 + i / 10));
            graphics.setColor(new Color(0 + i / 10, 0 + i / 10, 0 + i / 10));

            int height = (int) Math.round((double) distributionArray[i] / ((double) maxDistributionValue() / 500));

            graphics.drawRect(90 + (int)(500 / (1 / Main.step) * i), 590 - height, (int)(500 / (1 / Main.step)), height);
        }

        graphics.setColor(Color.BLACK);
        /*  Ось Y  */
        graphics.drawLine(90, 90, 90, 591);
        graphics.drawLine(90, 90, 85, 100);
        graphics.drawLine(90, 90, 95, 100);

        /*  Ось X  */
        graphics.drawLine(90, 591, 1120, 591);
        graphics.drawLine(1120, 591, 1110, 586);
        graphics.drawLine(1120, 591, 1110, 596);

        graphics.drawString(Integer.toString(maxDistributionValue()), 80, 80);

        graphics.drawString(Integer.toString(2), 1090, 615);
        graphics.drawLine(1090, 586, 1090, 596);

        graphics.drawString(Double.toString(1), 590, 615);
        graphics.drawLine(590, 586, 590, 596);

        graphics.drawString(Integer.toString(0), 90, 615);
    }

    public PaintDistribution(int[] array) {
        super("PaintDistribution");

        distributionArray = array;

        JFrame.setDefaultLookAndFeelDecorated(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setSize(1200, 700);
        setVisible(true);
    }
}
