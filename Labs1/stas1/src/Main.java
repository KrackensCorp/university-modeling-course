public class Main {
    public static int ITERATION_COUNT = 1000000;

    static double step = 0.025;
    static int[] arr = new int[(int) (2 / step)];

    private static void putValue(double value) {
        for (int j = 0; j < arr.length; j++) {
            if (value > j * step && value < (j + 1) * step) {
                arr[j]++;
            }
        }
    }

    public static int[] getArray() {
        double interval, y, ksi, X;

        for (int i = 0; i < ITERATION_COUNT; i++) {
            interval = Math.random();
            ksi = Math.random();

            if (interval >= 0 && interval < 0.125) {
                X = Math.sqrt(ksi);
                putValue(X);
            }

            if (interval >= 0.125 && interval <= 1)
            {
                if (ksi > 0.0575) {
                    X = (3 + 7 * Math.sqrt((10 * ksi) / 7 - 19 / 196)) / 5;
                    putValue(X);
                }
            }
        }

        for (int i = 0; i < arr.length; i++) {
            System.out.println((double) Math.round(i * step * 100) / 100 + " - " + (double) Math.round((i + 1) * step * 100) / 100 + ":\t\t " + arr[i]);
        }

        return arr;
    }

    public static int[] getArray2() {
        double x, y;

        for (int i = 0; i < ITERATION_COUNT; i++) {
            y = Math.random() * 2.5 - 1;

            if (y >= 0 && y < 0.125) {
                x = 2 * Math.sqrt(2.0) * Math.sqrt(y);
                putValue(x);
            }
            if (y >= -0.25 && y < 0.625)
            {
                x = (4 + Math.sqrt(40 * y + 11)) / 5;
                putValue(x);
            }
        }

        for (int i = 0; i < arr.length; i++) {
            System.out.println((double) Math.round(i * step * 100) / 100 + " - " + (double) Math.round((i + 1) * step * 100) / 100 + ":\t\t " + arr[i]);
        }

        return arr;
    }

    public static void main(String[] args) {
        PaintDistribution paint = new PaintDistribution(getArray());
    }
}